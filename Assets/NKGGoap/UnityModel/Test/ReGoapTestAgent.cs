﻿namespace ReGoap.Unity.Test
{
    /// <summary>
    /// 测试AI代理
    /// </summary>
    public class ReGoapTestAgent : ReGoapAgent<string, object>
    {
        public void Init()
        {
            Awake();
        }
    }
}