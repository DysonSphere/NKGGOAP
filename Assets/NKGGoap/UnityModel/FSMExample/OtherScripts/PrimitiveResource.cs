﻿using UnityEngine;

namespace ReGoap.Unity.FSMExample.OtherScripts
{
    /// <summary>
    /// 原始资源
    /// </summary>
    public class PrimitiveResource : Resource
    {
        /// <summary>
        /// 最小体积百分比
        /// </summary>
        public float MinScalePercentage = 0.1f;

        /// <summary>
        /// 起始体积
        /// </summary>
        private Vector3 startingScale;

        protected override void Awake()
        {
            base.Awake();
            startingScale = transform.localScale;
        }

        /// <summary>
        /// 移除资源（体积变小）
        /// </summary>
        /// <param name="value"></param>
        public override void RemoveResource(float value)
        {
            base.RemoveResource(value);
            transform.localScale = startingScale *
                                   (MinScalePercentage +
                                    (1f - MinScalePercentage) * (Capacity / startingCapacity)
                                   ); // scale down based on capacity
        }
    }
}